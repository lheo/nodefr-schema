#!/bin/bash

DEVHOST=lheo@dev.lheo.org
DEVPATH=www_dev/languages/nodefr2/1.0.0
DEV=${DEVHOST}:${DEVPATH}
RSYNC=rsync
SSH=ssh
SRC=doc/build/html

${SSH} ${DEVHOST} "mkdir -p ${DEVPATH}"
${RSYNC} --archive --verbose --exclude '*~' ${SRC}/. ${DEV}/.

