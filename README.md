# Compilation de la documentation 

Installation:

```bash
python3 -m venv $VENVDIR/venv_nodefr
source $VENVDIR/venv_nodefr/bin/activate
pip install -r requirements.txt
```

Fichiers d'entrée:

- Description des classes et éléments de données du fichier XLSX de travail.

```
NODEFR2="~/Dropbox/chrono_cons/2022/20220112-lheo-afnor-alignement/Description\ des\ classes\ et\ éléments\ de\ données\ \(format\ MLR-1\).xlsx"
```

- Fichier de correspondance entre les éléments de LHÉO et NoDEfr-2, obtenu lors de la construction du langage LHÉO (dans `lheo-schema`, faire `make`)

```
LHEONODEMAP=../lheo-schema/doc/source/nodefrmap.txt
```


Méthode générale pour bâtir la documentation:

```bash
# Création des fichiers *.rst dans doc/source
python build_doc.py --lheo $LHEONODEMAP $NODEFR2 doc/source
# Compilation de la documentation
(cd doc; make html)
# À ce stade, la doc est sous doc/build/html
# Publication sur dev.lheo.org/languages/nodefr2/1.0.0
./publish.sh
```
