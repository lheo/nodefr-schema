# Création d'une documentation pour NoDEFR-2

```bash
python3 -m venv ~/venv/nodefr-schema
source ~/venv/nodefr-schema/bin/activate
pip install -r requirements.txt
pip install -U pip
mkdir doc
```

```bash
cd doc/
sphinx-quickstart 
#> Séparer les répertoires build et source (y/n) [n]: y
#> Nom du projet: NoDEfr
#> Nom(s) de l'auteur: AFNOR
#> version du projet []: 0.2
#> Langue du projet [en]: fr
rm make.bat 
```

Méthode générale pour bâtir la documentation:

```bash
# Création des fichiers *.rst
python build_doc.py ~/Dropbox/chrono_cons/2022/20220112-lheo-afnor-alignement/Description\ des\ classes\ et\ éléments\ de\ données\ \(format\ MLR-1\).xlsx doc/source
# Compilation de la documentation
(cd doc; make html)
```

