import argparse
import re
import os
from collections import defaultdict

from openpyxl import load_workbook


class Binding(object):

    def __init__(self, target, info=None):
        self.target = target
        self.info = None


class DataElementSource(object):
    """Data Element Source.
    
    Source d'élément de données (SED).
    """

    def __init__(self, identifier, name, definition, langdep, codomain, content_rules, refine, examples,
                 notes, min_card, max_card, scheduling_reason, status, comments, rc):
        self.identifier = identifier
        self.name = name
        self.definition = definition
        self.langdep = langdep
        self.codomain = codomain
        self.codomain_rc = None
        self.content_rules = content_rules
        self.refine = refine
        self.examples = examples
        self.notes = notes
        self.min_card = min_card
        self.max_card = max_card
        self.scheduling_reason = scheduling_reason
        self.status = status
        self.comments = comments
        self.domain_rc = rc
        self.bindings = dict()
    
    sed_re = re.compile(r'(SED\d+)')

    @classmethod
    def from_ws_row(cls, row, rc):
        id = row[0]
        assert id is not None
        m = cls.sed_re.match(id)
        if m is not None:
            return cls(identifier=id, name=row[1], definition=row[2], langdep=row[3],
                    codomain=row[4], content_rules=row[5], refine=row[6], examples=row[7], notes=row[8],
                    min_card=row[9], max_card=row[10], scheduling_reason=row[11], status=row[12],
                    comments=row[13], rc=rc)
        return None

    def label(self):
        return '%s-desc' % self.identifier

    def __repr__(self):
        return '<%s %s>' % (self.identifier, self.name)

    def to_rst(self):
        s = []
        _s = s.append
        _s(".. _%s:" % self.label())
        _s("")
        if self.name:
            title = "%s %s" % (self.identifier, self.name)
        else:
            title = self.identifier
        _s(title)
        _s('=' * len(title))
        _s("")
        if self.definition:
            _s(self.definition)
            _s("")
        if self.langdep:
            _s("**Sensible à la langue.** %s" % self.langdep)
            _s("")
        if self.domain_rc:
            dom = ":ref:`%s`" % self.domain_rc.label()
            _s("**Domaine.** %s" % dom)
            _s("")
        if self.codomain:
            codom = "%s" % self.codomain
            if self.codomain_rc:
                codom = ":ref:`%s`" % self.codomain_rc.label()
            _s("**Codomaine.** %s" % codom)
            _s("")
        if self.content_rules:
            _s("**Règles de contenu.** %s" % self.content_rules)
            _s("")
        if self.refine:
            _s("**Raffine.** %s" % self.refine)
            _s("")
        if self.examples:
            exdata = self.examples
            if isinstance(exdata, str) and exdata.startswith("Exemple :"):
                exdata = exdata[9:]
            _s("**Exemples.** %s" % exdata)
            _s("")
        if self.notes:
            _s("**Notes.** %s" % self.notes)
            _s("")
        if self.min_card != '' or self.max_card != '':
            card = "%s,%s" % (self.min_card or '0', self.max_card or 'N')
            sched = ''
            if self.scheduling_reason:
                sched = ' (%s)' % sched
            _s("**Cardinalité.** %s%s" % (card, sched))
            _s("")
        if self.bindings:
            for binding_name, bindings in self.bindings.items():
                _s(".. admonition:: Implémentation %s" % binding_name)
                _s("")
                for binding in bindings:
                    _s("   - :ref:`%s`%s" % (binding.target, (" (%s)" % binding.info) if binding.info else ""))
                _s("")
        return '\n'.join(s)


class DataElementSourceContainer(object):
    """Data Element Sources."""

    def __init__(self, data_element_sources, rc):
        self.data_element_sources = data_element_sources
        self.resource_class = rc

    @classmethod
    def from_ws(cls, ws, rc, rc_container):
        data_element_sources = list()
        data_element_source_ids = set()
        for row in ws.values:
            id = row[0]
            if id is None:
                continue
            des = DataElementSource.from_ws_row(row, rc)
            if des is None:
                continue
            assert des.identifier not in data_element_source_ids, "%s in %s" % (des.identifier, rc.identifier)
            data_element_sources.append(des)
            data_element_source_ids.add(des.identifier)
        for des in data_element_sources:
            if des.codomain in rc_container.resource_classes:
                des.codomain_rc = rc_container.resource_classes[des.codomain]
                des.codomain_rc.codomain_of.append(des)
        return cls(data_element_sources, rc)

    def to_rst(self, s=None):
        if s is None:
            s = []
        _s = s.append
        if self.data_element_sources:
            m = "Sources d'éléments de données (SED)"
            _s("")
            _s("**%s.**" % m)
            #_s(m)
            #_s("-" * len(m))
            _s("")
            _s(".. toctree::")
            _s("   :maxdepth: 1")
            _s("")
            for des in self.data_element_sources:
                _s("   %s" % des.identifier)
        return '\n'.join(s)


class ResourceClass(object):
    """Resource Class (RC)."""

    def __init__(self, identifier, name, definition, subclass_of, examples, notes, abstract, status, comments,
                 iso_parent=None, rc_id=None):
        self.identifier = identifier
        self.rc_id = rc_id if rc_id is not None else identifier
        self.name = name
        self.definition = definition
        self.subclass_of = subclass_of
        self.examples = examples
        self.notes = notes
        self.abstract = abstract
        self.status = status
        self.comments = comments
        self.iso_parent = iso_parent
        self.parents = list()
        self.children = list()
        self.data_element_source_container = None
        self.codomain_of = list()

    iso_rc_re = re.compile(r'(?P<iso>ISO[^@]+)@(?P<rc>RC\d+)')
    rc_re = re.compile(r'(RC\d+)')

    @classmethod
    def from_ws_row(cls, row):
        id = row[0]
        assert id is not None
        m = cls.iso_rc_re.match(id)
        if m is not None:
            return cls(identifier=id, name=row[1], definition=row[2], subclass_of=row[3],
                       examples=row[4], notes=row[5], abstract=row[6], status=row[7], comments=row[8],
                       iso_parent=m.group('iso'), rc_id=m.group('rc'))
        m = cls.rc_re.match(id)
        if m is not None:
            return cls(identifier=id, name=row[1], definition=row[2], subclass_of=row[3],
                       examples=row[4], notes=row[5], abstract=row[6], status=row[7], comments=row[8],
                       rc_id=id)
        print(id)
        return None

    def label(self):
        return '%s-desc' % self.identifier
    
    def __repr__(self):
        return '<%s %s (%s)>' % (self.identifier, self.name, self.subclass_of if self.subclass_of else '')

    def to_rst(self, s=None):
        if s is None:
            s = []
        _s = s.append
        _s(".. _%s:" % self.label())
        _s("")
        if self.name:
            title = "%s %s" % (self.identifier, self.name)
        else:
            title = self.identifier
        _s(title)
        _s("=" * len(title))
        _s("")
        if self.definition:
            _s(self.definition)
            _s("")
        if self.parents:
            _s("**Classes parentes.**")
            _s("")
            for parent in self.parents:
                _s("- :ref:`%s`" % parent.label())
            _s("")
        if self.children:
            _s("**Classes enfants.**")
            _s("")
            for child in self.children:
                _s("- :ref:`%s`" % child.label())
            _s("")
        if self.codomain_of:
            _s("**Codomaine de.**")
            _s("")
            for des in self.codomain_of:
                _s("- :ref:`%s`" % des.label())
            _s("")
        if self.examples:
            exdata = self.examples
            if exdata.startswith("Exemple :"):
                exdata = exdata[9:]
            _s("**Exemples.** %s" % exdata)
            _s("")
        if self.notes:
            _s("**Notes.** %s" % self.notes)
            _s("")
        if self.abstract:
            _s("**Résumé.** %s" % self.abstract)
            _s("")
        if self.data_element_source_container:
            self.data_element_source_container.to_rst(s)
        _s("")
        return '\n'.join(s)


class ResourceClassContainer(object):

    def __init__(self):
        self.resource_classes = dict()

    @classmethod
    def from_ws(cls, ws):
        container = cls()
        for row in ws.values:
            id = row[0]
            if id is None:
                continue
            rc = ResourceClass.from_ws_row(row)
            if rc is None:
                continue
            assert rc.identifier not in container.resource_classes
            container.resource_classes[rc.identifier] = rc
        for rc in container.resource_classes.values():
            if rc.subclass_of:
                p_ids = map(lambda x: x.strip(), rc.subclass_of.split(','))
                for p_rc_id in p_ids:
                    assert p_rc_id in container.resource_classes, p_rc_id
                    parent_rc = container.resource_classes[p_rc_id]
                    rc.parents.append(parent_rc)
                    parent_rc.children.append(rc)
        return container


class Model(object):

    def __init__(self, resource_class_container, data_element_source_locations):
        self.resource_class_container = resource_class_container
        self.data_element_source_locations = data_element_source_locations

    @classmethod
    def from_wb(cls, wb):
        snames = wb.sheetnames
        assert "Classes" in snames
        ws_classes = wb['Classes']
        rc_container = ResourceClassContainer.from_ws(ws_classes)
        data_element_source_locations = dict()
        for rc_id, rc in rc_container.resource_classes.items():
            ws = wb[rc_id]
            des_container = DataElementSourceContainer.from_ws(ws, rc, rc_container)
            rc.data_element_source_container = des_container
            for des in des_container.data_element_sources:
                assert des.identifier not in data_element_source_locations, \
                    "%s (%s) already in %s" % (
                        des.identifier, rc.identifier, data_element_source_locations[des.identifier].identifier)
                data_element_source_locations[des.identifier] = rc
        return cls(rc_container, data_element_source_locations)

    def to_rst(self, s=None):
        if s is None:
            s = []
        _s = s.append
        title = "NoDEfr-2"
        _s(title)
        _s("=" * len(title))
        _s("")
        _s(".. toctree::")
        _s("   :maxdepth: 3")
        _s("   :caption: Contenu:")
        _s("")
        for rc in self.resource_class_container.resource_classes.values():
            _s("   %s" % rc.identifier)
        _s("")
        return '\n'.join(s)
        
    def generate_rst_files(self, target_dir):
        for rc in self.resource_class_container.resource_classes.values():
            with open(os.path.join(target_dir, '%s.rst' % rc.identifier), 'w', encoding='utf-8') as f:
                f.write(rc.to_rst())
                for des in rc.data_element_source_container.data_element_sources:
                    with open(os.path.join(target_dir,'%s.rst' % des.identifier), 'w', encoding='utf-8') as des_f:
                        des_f.write(des.to_rst())
        with open(os.path.join(target_dir, 'index.rst'), 'w', encoding='utf-8') as f:
            f.write(self.to_rst())

    def apply_lheo_map(self, lheonodefrmap_fname):
        des_map = defaultdict(list)
        with open(lheonodefrmap_fname, 'r') as f:
            for line in f:
                if not line:
                    continue
                elname, cercle, des = line.split('|')
                des_list = list(map(lambda x: x.strip(), des.split(',')))
                for des_id in des_list:
                    des_map[des_id].append(Binding(elname, cercle))
        for des_id, bindings in des_map.items():
            rc = self.data_element_source_locations.get(des_id)
            if rc is not None:
                des = None
                for des_cand in rc.data_element_source_container.data_element_sources:
                    if des_cand.identifier == des_id:
                        des = des_cand
                        break 
                assert des is not None
                des.bindings['LHÉO 2.3'] = bindings


def main():
    #default_name="Description des classes et éléments de données (format MLR-1).xlsx"
    #lheo_align="LHEO 2.3 <-> NodeFR-2.xlsx"
    parser = argparse.ArgumentParser(description='Transform XLSX file to doc.')
    parser.add_argument("--lheo")
    parser.add_argument('source_file')
    parser.add_argument('target_dir')
    args = parser.parse_args()
    fname = args.source_file
    target_dir = args.target_dir
    lheonodefrmap_fname = args.lheo
    wb = load_workbook(fname)
    node_fr2 = Model.from_wb(wb)
    if lheonodefrmap_fname:
        node_fr2.apply_lheo_map(lheonodefrmap_fname)
    node_fr2.generate_rst_files(target_dir)
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main())

